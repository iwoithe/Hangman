# Hangman

A free and open source hangman game which can be played in your terminal.

## Code guidelines

- class names: TitleCase
- methods/functions: snake_case
- variables: snake_case

## Support

If you have found a bug or want to request for a new feature please open an issue on either GitLab or GitHub.

## License

Hangman is released under the GNU General Public License v3.
